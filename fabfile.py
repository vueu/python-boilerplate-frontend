"""
FRONTENT DEPLOYMENT
"""
from os import path, chdir as _chdir, system, getenv

from fabric.api import run, local, lcd, cd, put, env
from fabric.operations import sudo  # prompt, get

env.use_ssh_config = True
here = path.abspath(path.dirname(__file__))
_chdir(here)

# Default hosts
if not env.hosts:
    env.hosts = ["jyn"]

DIR_REMOTE = "/server/websites/python-boilerplate.com/htdocs/"
DIR_REMOTE_TEST = "/server/websites/python-boilerplate.com/subdomains/test/htdocs/"

def build():
    local("rm -rf dist")
    local("npm run build")

    print("Generating sitemap.xml")
    system("node scripts/generate-sitemap.js > dist/sitemap.xml")

def deploy(skipBuild=False):
    if getenv("SKIP_PRERENDER"):
        print("ERROR: Deployment without prerendering (SKIP_PRERENDER environment variable is set)")
        print("Don't do this! The old js files will be referenced.")
        exit(1)

    if not skipBuild:
        build()

    # Pack
    with lcd("dist"):
        local("tar -czf /tmp/pybp-frontend.tar.gz *")

    # Upload
    put("/tmp/pybp-frontend.tar.gz", "/tmp/pybp-frontend.tar.gz")

    # Extract
    with cd(DIR_REMOTE):
        sudo("tar -xf /tmp/pybp-frontend.tar.gz", user='www-data')
        run("rm -f /tmp/pybp-frontend.tar.gz")

    # Cleanup
    local("rm -f /tmp/pybp-frontend.tar.gz")
