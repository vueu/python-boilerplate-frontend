// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import store from './store'
import VueRouter from 'vue-router'
import VueHighlightJS from 'vue-highlightjs'
import VueAnalytics from 'vue-analytics'

import { router } from './router'
import { sync } from 'vuex-router-sync'
import { GOOGLE_ANALYTICS_UA } from './settings'

import App from './App'

// vue-awesome: https://github.com/Justineo/vue-awesome
import VueAwesome from 'vue-awesome'
Vue.component('icon', VueAwesome)

// vue-router
Vue.use(VueRouter)

// Setup vuex router sync
sync(store, router)

// Other Vue extensions
Vue.use(VueHighlightJS)
Vue.use(VueAnalytics, {
  id: GOOGLE_ANALYTICS_UA,
  router,
  autoTracking: {
    exception: true
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
