import VueRouter from 'vue-router'

import Home from './routes/Home'
import About from './routes/About'
import NotFound from './routes/NotFound'

export const routes = [
  { path: '/', redirect: '/py3+executable' },
  { name: 'error', path: '/error', component: Home },
  { path: '/about', component: About },
  { name: 'boilerplate', path: '/:bpConfig', component: Home },
  { path: '*', component: NotFound }
]

// Setup router
export const router = new VueRouter({
  mode: 'history',
  routes
})
